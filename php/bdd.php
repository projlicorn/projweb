<?php

$servername = "localhost";
$username = "root";
$password = "";

$dbName = "projetweb";
$adminTable = "admin";
$adminListingTable = "adminListing";
$userTable = "user";
$emploiTable ="emploi";
$publicationTable = "publication";
$albumListingTable = "albumListing";
$albumTable = "album";
$mediaTable = "media";
$eventListingTable = "eventListing";
$eventTable = "event";
$postTable = "post";
$friendshipTable = "friendship";
$groupeListingTable = "groupeListing";
$groupeTable = "groupe";
$messageTable = "message";
$commentTable = "comment";
$likeTable = "jaime";
$unlikeTable = "jaimepas";
$loveTable = "love";
$fireTable = "fire";

//connect server
$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//create database
if (!$conn->query("CREATE database IF NOT EXISTS ". $dbName)){
    echo "Error creating database: " . $conn->error;
}

//connect the database
if (!$conn->query("USE ". $dbName)){
    echo "Error using database: " . $conn->error;
}

//create table admin
$tab = "create table IF NOT EXISTS " . $adminTable . "(
admin_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
admin_mail VARCHAR(30) NOT NULL,
admin_password VARCHAR(30) NOT NULL
)";

if (!$conn->query($tab)){
    echo "Error creating table1: " . $conn->error;
}

//create table admin
$tab = "create table IF NOT EXISTS " . $adminListingTable . "(
admin_id INT(5) UNSIGNED,
user_id INT(5) UNSIGNED,
PRIMARY KEY(admin_id,user_id)
)";

if (!$conn->query($tab)){
    echo "Error creating table2: " . $conn->error;
}


//create table user
$tab = "create table if not exists " . $userTable . "(
user_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
mail VARCHAR(30) NOT NULL,
pseudo VARCHAR(30) NOT NULL,
name VARCHAR(30) NOT NULL,
firstname VARCHAR(30) NOT NULL,
pp_id INT(5) UNSIGNED,
couv_id INT(5) UNSIGNED,
user_description VARCHAR(140)
)";

if(!$conn->query($tab))
    echo "Error creating " . $conn->error;


//create table
$tab = "create table if not exists " . $publicationTable . "(
publication_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT(5) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table4: " . $conn->error;

//create table
$tab = "create table if not exists " . $albumTable . "(
album_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
album_name VARCHAR(30) NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table5: " . $conn->error;

//create table
$tab = "create table if not exists " . $mediaTable . "(
media_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT(5) UNSIGNED NOT NULL,
media_src VARCHAR(60) NOT NULL,
media_text VARCHAR(140) NOT NULL,
media_date VARCHAR(30) NOT NULL,
media_place VARCHAR(30) NOT NULL,
media_feeling VARCHAR(30) NOT NULL,
media_config INT(1) UNSIGNED NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table6: " . $conn->error;

//create table
$tab = "create table if not exists " . $albumListingTable . "(
album_id INT(10) UNSIGNED,
media_id INT(10) UNSIGNED,
PRIMARY KEY(album_id,media_id),
user_id INT(5) UNSIGNED NOT NULL,
album_date VARCHAR(30) NOT NULL,
album_place VARCHAR(30) NOT NULL,
album_feeling VARCHAR(30) NOT NULL,
album_config INT(1) UNSIGNED NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table7: " . $conn->error;


//create table
$tab = "create table if not exists " . $postTable . "(
post_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
post_text VARCHAR(140) NOT NULL,
user_id INT(5) UNSIGNED NOT NULL,
post_date VARCHAR(30) NOT NULL,
post_place VARCHAR(30) NOT NULL,
post_feeling VARCHAR(30) NOT NULL,
post_config INT(1) UNSIGNED NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table8: " . $conn->error;

//create table
$tab = "create table if not exists " . $eventTable . "(
event_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
event_date VARCHAR(30) NOT NULL,
event_name VARCHAR(30) NOT NULL,
event_text VARCHAR(140) NOT NULL,
publication_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table9: " . $conn->error;

//create table
$tab = "create table if not exists " . $eventListingTable . "(
event_id INT(10) UNSIGNED,
user_id INT(5) UNSIGNED,
PRIMARY KEY(event_id,user_id)
)";


if (!$conn->query($tab))
    echo "Error creating table10: " . $conn->error;

//create table
$tab = "create table if not exists " . $friendshipTable . "(
  user_id_1 INT(5) UNSIGNED,
  user_id_2 INT(5) UNSIGNED,
  PRIMARY KEY(user_id_1,user_id_2),
  invit_status INT(1) UNSIGNED NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table11: " . $conn->error;

//create table
$tab = "create table if not exists " . $groupeListingTable . "(
  user_id INT(5) UNSIGNED,
  groupe_id INT(5) UNSIGNED,
  PRIMARY KEY(user_id,groupe_id)
)";


if (!$conn->query($tab))
    echo "Error creating table12: " . $conn->error;

//create table
$tab = "create table if not exists " . $groupeTable . "(
groupe_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
groupe_name VARCHAR(30) NOT NULL
)";


if (!$conn->query($tab))
    echo "Error creating table9: " . $conn->error;


//create table
$tab = "create table if not exists " . $messageTable . "(
  message_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  message_text VARCHAR(140) NOT NULL,
  groupe_id INT(5) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;

//create table
$tab = "create table if not exists " . $commentTable . "(
  comment_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(5) UNSIGNED NOT NULL,
  comment_text VARCHAR(140) NOT NULL,
  comment_date VARCHAR(30) NOT NULL,
  post_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;


//create table
$tab = "create table if not exists " . $likeTable . "(
  like_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(5) UNSIGNED NOT NULL,
  post_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;

//create table
$tab = "create table if not exists " . $unlikeTable . "(
  unlike_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(5) UNSIGNED NOT NULL,
  post_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;

//create table
$tab = "create table if not exists " . $loveTable . "(
  love_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(5) UNSIGNED NOT NULL,
  post_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;

//create table
$tab = "create table if not exists " . $fireTable . "(
  fire_id INT(5) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id INT(5) UNSIGNED NOT NULL,
  post_id INT(10) UNSIGNED
)";


if (!$conn->query($tab))
    echo "Error creating table14: " . $conn->error;

//create table emploi
//$tab = "create table if not exists " . $emploiTable."(
//emploi_id INT(4) UNSIGNED AUTO INCREMENT PRIMARY KEY,
//emploi_text VARCHAR(30) NOT NULL
//)";

//if (!$conn->query($tab))
//    echo "Error creating table14: " . $conn->error;


?>
