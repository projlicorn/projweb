<?php

require('connect.php');

//retourne tous les administrateurs
function getAllAdmin($db, $table){
  $select = "SELECT * FROM " . $table;
  $tab = array();

  if ($result = $db->query($select)) {

    while ($row = $result->fetch_assoc()) {
      $tab = array_merge($tab, array($row['admin_mail']=>$row['admin_password']));
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
  
  return $tab;
}

foreach(getAllAdmin($conn, $adminTable) as $mail => $password)
  if(htmlspecialchars($_POST['mail-admin']) == $mail)
    if(htmlspecialchars($_POST['password-admin']) == $password)
      //conection succeed
      header('Location: ../templates/admin.php');


?>