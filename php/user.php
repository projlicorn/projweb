<?php

//session_start();
require('connect.php');

function printAllFriends($db, $id, $option=""){
  $select = "SELECT F.user_id_1, F.user_id_2, F.invit_status, U.user_id, U.name, U.firstname, U.pseudo, U.mail FROM friendship F, user U WHERE invit_status=1 AND (U.user_id = F.user_id_2 AND user_id_1=" . $id . " OR U.user_id = F.user_id_1 AND user_id_2=" . $id .")" . $option;
  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      echo '<li><a href="../templates/journal.php?id='.$row['user_id'] .'#journal" onclick="disableUserOny('.$id.','.$_SESSION['id'].');">' . $row['firstname'];
      echo ' ' . $row['name'] . '</a>';
      echo ' (' . $row['pseudo'] . ') <button type="button" onclick="deleteFriendFunction('.$row['user_id'].')" class="btn btn-default fl-right"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></li>';
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printPosts($db, $table, $id){
  $select = "SELECT * from " . $table . " WHERE user_id=" . $id . " ORDER BY post_date DESC";

  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()){
      //si le profil est le sien OU si la config est en 'amis seulement & que l'user connecté est ami avec le profil OU que la config est en public -> affichage du post
      if($id==$_SESSION['id'] || $row['post_config'] == 2 && areFriends($db, $id, $_SESSION['id']) || $row['post_config']==3){
        echo '<li><div class="panel panel-default"><div class="panel-heading post-title"><h3 class="panel-title">Le ' . $row['post_date'];
        echo ', à ' . $row['post_place'];
        echo '</h3><div class="dropdown content__box__post-button user__only"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span></button><ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1"><li><a href="#">Confidentialité</a></li><li><a href="#">Modifier</a></li><li><a href="#" onclick="deletePostFunction('.$row['post_id'].')">Supprimer</a></li></ul></div></div><div class="panel-body content__box__post__contenu"><div class="content__box__post__contenu-text"><div class="post__feeling">Humeur : Je suis ' . $row['post_feeling'];
        echo '</div><div class="post__text">' . $row['post_text'];
        echo '</div></div></div><div class="panel-footer"><div class="content__box__post__contenu__reaction"><div><ul class="comment-ul" id="user-comment-list-'.$row['post_id'].'">';

        printComments($db, 'comment', $id, $row['post_id']);
        echo'</ul></div><div class="content__box__post__contenu__reaction-comment"><div class="input-group"><input type="text" class="form-control" placeholder="Commenter..." id="user-comment-input-'.$row['post_id'].'"><span class="input-group-btn"><button class="btn btn-default" type="button" onclick="addCommentFunction('.$row['post_id'].');"><span class="glyphicon glyphicon-send" aria-hidden="true"></span></button></span></div></div><div class="content__box__post__contenu__reaction-like"><div class="btn-group" role="group" aria-label="..."><button onclick="addLikeFunction('.$row['post_id'].');" type="button" class="btn btn-default"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"><span id="user-like-input-'.$row['post_id'].'" class="badge">';
        printLikes($db, 'jaime', $_SESSION['id'], $row['post_id']);
        echo '</span></span></button><button onclick="addUnlikeFunction('.$row['post_id'].');" type="button" class="btn btn-default"><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"><span id="user-unlike-input-'.$row['post_id'].'" class="badge">';
        printUnlikes($db, 'jaimepas', $_SESSION['id'], $row['post_id']);
        echo '</span></span></button><button onclick="addLoveFunction('.$row['post_id'].');" type="button" class="btn btn-default"><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"><span id="user-love-input-'.$row['post_id'].'" class="badge">';
        printLoves($db, 'love', $_SESSION['id'], $row['post_id']);
        echo '</span></span></button><button onclick="addFireFunction('.$row['post_id'].');" type="button" class="btn btn-default"><span class="glyphicon glyphicon-fire" aria-hidden="true"><span id="user-fire-input-'.$row['post_id'].'" class="badge">';
        printFires($db, 'fire', $_SESSION['id'], $row['post_id']);
        echo '</span></span></button></div></div></div></div></div></li>';
      }

    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printInfo($db, $id){
  $select = "SELECT * from user WHERE user_id=" . $id;

  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      echo $row['user_description'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printComments($db, $table, $user, $post){
  $select = "SELECT C.comment_text, C.comment_date, U.name, U.firstname, U.user_id from comment C, user U WHERE C.user_id=U.user_id AND C.post_id=".$post." ORDER BY comment_date";

  if ($result = $db->query($select)){
    while ($row = $result->fetch_assoc()) {
      echo '<li class="comment-li""><div class="comment-text"><a href="../templates/journal.php?id='.$row['user_id'].'#journal"><span class="user-nom">'.$row['firstname'].' '.$row['name'].'</span></a> '.$row['comment_text'].'</div><div class="comment-date">Le '.$row['comment_date'].'</div></li>';
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printLikes($db, $table, $user, $post){
  $select = "SELECT COUNT(like_id) FROM ". $table ." WHERE post_id=".$post;
  if ($result = $db->query($select)){
    while ($row = $result->fetch_assoc()) {
      echo $row['COUNT(like_id)'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printUnlikes($db, $table, $user, $post){
  $select = "SELECT COUNT(unlike_id) FROM ". $table ." WHERE post_id=".$post;
  if ($result = $db->query($select)){
    while ($row = $result->fetch_assoc()) {
      echo $row['COUNT(unlike_id)'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printLoves($db, $table, $user, $post){
  $select = "SELECT COUNT(love_id) FROM ". $table ." WHERE post_id=".$post;
  if ($result = $db->query($select)){
    while ($row = $result->fetch_assoc()) {
      echo $row['COUNT(love_id)'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printFires($db, $table, $user, $post){
  $select = "SELECT COUNT(fire_id) FROM ". $table ." WHERE post_id=".$post;
  if ($result = $db->query($select)){
    while ($row = $result->fetch_assoc()) {
      echo $row['COUNT(fire_id)'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function printAllPhotos($db, $table, $id){
  $select = "SELECT * FROM " . $table . " WHERE user_id=" . $id;

  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      if($id==$_SESSION['id'] || $row['media_config']==2 && areFriends($db, $id, $_SESSION['id'])||$row['media_config'] == 3){
        echo '<a href="#" class="gallery-item"><div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive img-thumbnail" src="'.htmlspecialchars($row['media_src']).'"/></div></a>';
      }
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function getCarouselPhotos($db, $table, $id){
  $select = "SELECT * FROM " . $table . " WHERE user_id=" . $id;

  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      if($id==$_SESSION['id'] || $row['media_config']==2 && areFriends($db, $id, $_SESSION['id'])||$row['media_config'] == 3){
        echo '<div class="item carousel-item"><img src="'. htmlspecialchars($row['media_src']).'" alt="carousel"></div>';
      }
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function getPPSrc($db, $id){
  $src=NULL;
  $select = "SELECT media.media_src from user, media WHERE user.user_id=".$id." AND media.user_id=".$id." AND media.media_id=user.pp_id";
    if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      $src = $row['media_src'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;

  if($src == NULL)
    return '../img/avatar.jpg';
  else
    return $src;
}

function getCouvSrc($db, $id){
  $src=NULL;
  $select = "SELECT media.media_src from user, media WHERE user.user_id=".$id." AND media.user_id=".$id." AND media.media_id=user.couv_id";
    if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      $src = $row['media_src'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;

  if($src == NULL)
    return '../img/avatar.jpg';
  else
    return $src;
}

function getNames($db, $id){
  $select = "SELECT * from user WHERE user_id=".$id;
  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      echo $row['firstname']. ' ' .$row['name'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function getPseudo($db, $id){
  $select = "SELECT * from user WHERE user_id=".$id;
  if ($result = $db->query($select)) {
    while ($row = $result->fetch_assoc()) {
      echo $row['pseudo'];
    }
    $result->free();
  }
  else echo 'Error : ' .$db->error;
}

function areFriends($db, $id1, $id2){
  $select = "SELECT * from friendship WHERE user_id_1=".$id1." AND user_id_2=".$id2." OR user_id_2=".$id1." AND user_id_1=".$id2;
  if ($result = $db->query($select)){
    if($result->num_rows >0)
      return true;
  }
  return false;
}

?>
