<?php

$servername = "localhost";
$username = "root";
$password = "";

$dbName = "projetweb";
$adminTable = "admin";
$adminListingTable = "adminListing";
$userTable = "user";
$publicationTable = "publication";
$albumListingTable = "albumListing";
$albumTable = "album";
$mediaTable = "media";
$eventListingTable = "eventListing";
$eventTable = "event";
$postTable = "post";
$friendshipTable = "friendship";
$groupeListingTable = "groupeListing";
$groupeTable = "groupe";
$messageTable = "message";
$commentTable = "comment";
$likeTable = "jaime";
$unlikeTable = "jaimepas";
$loveTable = "love";
$fireTable = "fire";

//connect server
$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//connect the database
if (!$conn->query("USE ". $dbName)){
    echo "Error using database: " . $conn->error;
}

?>
